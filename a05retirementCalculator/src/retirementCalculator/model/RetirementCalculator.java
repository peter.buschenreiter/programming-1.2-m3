package retirementCalculator.model;

public class RetirementCalculator {
	private int birthYear;
	public static final int RETIREMENT_AGE = 67;

	public RetirementCalculator() {
	}

	public String getRetirementYear() {
		return Integer.toString(birthYear + RETIREMENT_AGE);
	}

	public int getBirthYear() {
		return birthYear;
	}

	public void setBirthYear(int birthYear) {
		this.birthYear = birthYear;
	}
}
