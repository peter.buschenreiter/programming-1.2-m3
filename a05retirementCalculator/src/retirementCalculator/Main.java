package retirementCalculator;

import retirementCalculator.model.RetirementCalculator;
import retirementCalculator.view.RetirementCalculatorPresenter;
import retirementCalculator.view.RetirementCalculatorView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		RetirementCalculator model = new RetirementCalculator();
		RetirementCalculatorView view = new RetirementCalculatorView();

		RetirementCalculatorPresenter presenter = new RetirementCalculatorPresenter(model, view);
		primaryStage.setScene(new Scene(view));
		primaryStage.setTitle("Retirement Calculator");
		primaryStage.setWidth(400);
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
