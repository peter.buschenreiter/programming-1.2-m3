package retirementCalculator.view;

import retirementCalculator.model.RetirementCalculator;

public class RetirementCalculatorPresenter {
	private final RetirementCalculator model;
	private final RetirementCalculatorView view;

	public RetirementCalculatorPresenter(RetirementCalculator model, RetirementCalculatorView view) {
		this.model = model;
		this.view = view;

		addEventHandlers();
//		updateView();
//		view.getRetirementYearLabel().setText("");
	}

	private void addEventHandlers() {
		view.getButton().setOnAction(actionEvent -> {
			try {
				int year = Integer.parseInt(view.getTextField().getText());
				model.setBirthYear(year);
				updateView();
			} catch (NumberFormatException e) {
				view.getRetirementYearLabel().setText("Please enter a number!");
			}
		});
	}

	private void updateView() {
		view.getRetirementYearLabel().setText(model.getRetirementYear());
	}
}
