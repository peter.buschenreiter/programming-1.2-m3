package retirementCalculator.view;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

public class RetirementCalculatorView extends GridPane {
	private Button button;
	private TextField textField;
	private Label birthYearLabel;
	private Label retirementYearLabel;

	public RetirementCalculatorView() {
		initializeNodes();
		layoutNodes();
	}

	private void initializeNodes() {
		button = new Button("Birth Year -> Retirement Year");
		textField = new TextField();
		textField.setPrefColumnCount(4);
		retirementYearLabel = new Label();
		birthYearLabel = new Label("Birth Year");
		birthYearLabel.setLabelFor(textField);
	}

	private void layoutNodes() {
		int i = 0;
		setHgap(10);
		add(birthYearLabel, 0, 0);
		add(textField, 0, 1);
		add(button, 1, 1);
		add(retirementYearLabel, 2, 1);
		retirementYearLabel.setMinWidth(100);
		retirementYearLabel.setPadding(new Insets(4));
		retirementYearLabel.setBorder(new Border(new BorderStroke(new Color(0, 0, 0, 1), BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
		setPadding(new Insets(10));
	}

	Button getButton() {
		return button;
	}

	TextField getTextField() {
		return textField;
	}

	Label getRetirementYearLabel() {
		return retirementYearLabel;
	}

	Label getBirthYearLabel() {
		return birthYearLabel;
	}
}
