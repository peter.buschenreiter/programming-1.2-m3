package be.kdg.dice.model;

import java.util.Random;

public class Dice {
    public static final int MAX_PIPS = 6;

    private int valueDie1;
    private int valueDie2;
    private final Random random;

    public Dice() {
        this.random = new Random();
        throwDice();
    }

    public void throwDice(){
        valueDie1 = random.nextInt(MAX_PIPS) + 1;
        valueDie2 = random.nextInt(MAX_PIPS) + 1;
    }

    public int getValueDie1() {
        return valueDie1;
    }

    public int getValueDie2() {
        return valueDie2;
    }
}
