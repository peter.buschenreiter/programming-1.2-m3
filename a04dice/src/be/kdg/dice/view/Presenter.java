package be.kdg.dice.view;

import be.kdg.dice.model.Dice;
import javafx.scene.image.Image;

public class Presenter {
	private Dice model;
	private DiceView view;

	public Presenter(Dice model, DiceView view) {
		this.model = model;
		this.view = view;
		addEventHandlers();
		updateView();
	}

	private void addEventHandlers() {
		view.getButton().setOnAction(actionEvent -> {
			model.throwDice();
			updateView();
		});
	}

	private void updateView() {
		int die1 = model.getValueDie1();
		int die2 = model.getValueDie2();
		view.getDie1().setImage(new Image("die" + die1 + ".png"));
		view.getDie2().setImage(new Image("die" + die2 + ".png"));
	}
}
