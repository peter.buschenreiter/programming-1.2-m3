package be.kdg.dice.view;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;


public class DiceView extends GridPane {

	private ImageView die1;
	private ImageView die2;
	private Button button;

	public DiceView() {
		initialiseNodes();
		layoutNodes();
	}

	private void layoutNodes() {
		setPadding(new Insets(10));
		setVgap(10);
		setHgap(10);
		add(die1, 0, 0);
		add(die2, 1, 0);
		add(button, 0, 1);
		setColumnSpan(button, 2);
		setHalignment(button, HPos.CENTER);
	}

	private void initialiseNodes() {
		die1 = new ImageView("die1.png");
		die2 = new ImageView("die1.png");
		button = new Button("Roll");
		button.setPrefWidth(80);
	}

	ImageView getDie1() {
		return die1;
	}

	ImageView getDie2() {
		return die2;
	}

	Button getButton() {
		return button;
	}
}
